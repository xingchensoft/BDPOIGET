﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static DBPOIGET.Classes.POIs;
using System.Data;
using System.IO;

namespace DBPOIGET.Classes
{
    public class Export
    {
        DataTable _table;

        public Export(Dictionary<string, Poi> poidic)
        {
            _table = new DataTable();
            _table.Columns.Add("uid");
            _table.Columns.Add("name");
            _table.Columns.Add("lng");
            _table.Columns.Add("lat");
            _table.Columns.Add("address");
            //_table.Columns.Add("telephone");
            _table.Columns.Add("type");
            _table.Columns.Add("price");
            _table.Columns.Add("overall_rating");
            _table.Columns.Add("taste_rating");
            _table.Columns.Add("service_rating");
            _table.Columns.Add("environment_rating");
            foreach (var poi in poidic.Values)
            {
                DataRow dataRow = _table.NewRow();
                dataRow["uid"] = poi.uid;
                dataRow["name"] = poi.name.Replace(",", "，");
                dataRow["lng"] = poi.location.lng;
                dataRow["lat"] = poi.location.lat;
                dataRow["address"] = poi.address.Replace(",","，");
                //dataRow[5] = poi.telephone;
                dataRow["type"] = poi.detail_info.tag;
                dataRow["price"] = poi.detail_info.price;
                dataRow["overall_rating"] = poi.detail_info.overall_rating;
                dataRow["taste_rating"] = poi.detail_info.taste_rating;
                dataRow["service_rating"] = poi.detail_info.service_rating;
                dataRow["environment_rating"] = poi.detail_info.environment_rating;
                _table.Rows.Add(dataRow);
            }
        }

        /// <summary>
        /// 保存
        /// </summary>
        /// <param name="fileName"></param>
        public void Save(string fileName)
        {
            Stream myStream = File.Open(fileName, FileMode.OpenOrCreate, FileAccess.ReadWrite);
            StreamWriter sw = new StreamWriter(myStream, Encoding.GetEncoding(-0));

            string str = "";
            for (int i = 0; i < _table.Columns.Count; i++)
            {
                if (i > 0)
                {
                    str += ",";
                }
                str += _table.Columns[i].ColumnName;
            }
            sw.WriteLine(str);
            for (int i = 0; i < _table.Rows.Count - 1; i++)
            {
                string tempStr = "";
                for (int j = 0; j < _table.Columns.Count; j++)
                {
                    if (j > 0)
                    {
                        tempStr += ",";
                    }
                    tempStr += _table.Rows[i][j].ToString().Replace("\r", "").Replace("\n", "");
                }
                sw.WriteLine(tempStr);
            }
            sw.Close();
            myStream.Close();
        }
    }
}
